package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations 
{	
	//-------------------------------------------------------
	// Métodos
	//-------------------------------------------------------
	
	public double computeMean(IntegersBag bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	/**
	 * Retorna el valor más grandee entre los elementos de la muestra. 
	 * @param bag es la muestra de enteros. 
	 * @return Máximo valor encontrado. 
	 */
	public int getMax(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value)
				{
					max = value;
				}
			}			
		}
		return max;
	}
	
	/**
	 * Retorna el valor más pequeño entre los elementos de la muestra. 
	 * @param bag es la muestra de enteros. 
	 * @return Mínimo valor encontrado. 
	 */
	public int getMin( IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( min > value)
				{
					min = value;
				}
			}			
		}
		return min;
	}
}
